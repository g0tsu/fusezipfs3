# fusezipfs3

> Mount zip files as a regular filesystem.

## Table of contents

  * [Introduction](#introduction)
  * [Features](#features)
  * [Limitations](#limitations)
  * [How To Install](#how-to-install)
  * [Usage Example](#usage-example)
    - [How to mount](#how-to-mount)
    - [How to unmount](#hot-to-unmount)
  * [License](#license)

## Introduction

The `fusezipfs3` was made to help me with `zip` archives management which I
use to store some resource files and constantly updated stuff. I didn't want to
use those additional scripts and code to search, extract and insert the file 
back any time I wanted to change a single line in a single file that lies in a `zip` archive.
This is why the `fusezipfs3` was born, and now, my beloved `find` and `sed` doesn't even 
know that input files are archive's entries.

## Features

  - Read/Write support
  - UTF-8
  - Zero dependencies (only fuse3)
  - Entirely written in `C`

## Limitations

 Since it is a zip archive there are some limitations. You can't use a
 regular filesystem operations such as symlinks or any kind of links,
 change permissions and timestamps.
 Write operations are singlethreded othrewise it would ruine the entire zip
 archive every time changes applied simultaniously. Which is why it copies
 files one by one. Anyways it works fast enough.

## How To install

The build and installation process is simple as:

```
./bootstrap
./configure
make
make install
```

## Usage Example

### How to mount
Lets say we would like to create a new zip file and copy something
into it.

```
mkdir zipdir
fusezipfs3 ./zipfile.zip ./zipdir
cp ~/Documents/cat-names.txt ./zipdir
```

`zipfile.zip` was created and mounted into `./zipdir` directory. Then,
using `cp` command we coppy our `cat-names.txt` into archive. It is simple
as working with a regular filesystem.

### How to unmount

`libfuse3` itself provides `fusermount3` command, just use it:

```
fusermount3 -u ./zipdir
```
The `-u` option of `fusermount3`stands for `unmount`.

## License

GPLv2

```

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 Author: g0tsu
 Email:  g0tsu at dnmx.0rg
```

