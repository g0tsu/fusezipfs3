/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#ifndef __ZIPFS_H__
#define __ZIPFS_H__

#include <pthread.h>
#include <stdint.h>
#include <time.h>

#define ZIPFS_ENTRIES_UPDATE_TIMEOUT 10

struct zentry {
  char *name;
  char *parent;
  char *orig;
  int name_len;
  int parent_len;
  int orig_len;
  uint32_t size;
  uint8_t isdir;
};

struct zint_arr {
  int **arr;
  int size;
};

struct zipfs {
  pthread_mutex_t lock;
  char *zipfile;
  struct zentry **entries;
  int entries_size;
  char *mountpoint;
  char *tmpdir;
  int verbose;
  int debug;
  time_t last_update;
};

extern struct zipfs zipfs;

enum {
  KEY_TMPDIR,
  KEY_HELP,
  KEY_VERBOSE,
  KEY_VERSION,
};

void zipfs_fill_entries(void);
void zipfs_free_entries(void);
struct zint_arr *zipfs_find_entries(const char *str, const int len);
void zipfs_free_zint_arr(struct zint_arr *o);

char *zipfs_get_entry_parent(const char *str, const int len);
char *zipfs_get_entry_name(const char *str, const int len);
#endif

